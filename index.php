<?php
define( '_VALID_ACCESS', 1 );

// ________________________________________________________________

include ("model.php");
include ("parser.php");

define('LINE_FEED', '<br/>');

// ________________________________________________________________

$target = isset($_REQUEST["target"]) ? $_REQUEST["target"] : "";
if ($target == "speichern") {
	rename(CFG::DATEINAME_TEMP(), CFG::DATEINAME());
	header('Location: ./index.php');
	exit;
}

doit($target == 'cron');

// ________________________________________________________________

function doit($isCron) {
	$aenderungVorhanden = false;
	$output = "J!Watch".LINE_FEED.LINE_FEED;
	$output .= '<a href="https://bitbucket.org/SammyIT/jwatch/wiki/Home">More information: Wiki</a>'.LINE_FEED;
	$output .= '<a href="https://bitbucket.org/SammyIT/jwatch/issues?status=new&status=open">Issue-Tracker</a>'.LINE_FEED;
	$output .= '<a href="http://flattr.com/thing/580766/JWatch" target="_blank"><img src="http://api.flattr.com/button/flattr-badge-large.png" alt="Flattr this" title="Flattr this" border="0" /></a>'.LINE_FEED;
	$output .= LINE_FEED;

	$dataLocal = @file_get_contents(CFG::DATEINAME());
	if ($dataLocal === FALSE) {
		$output .= 'Keine gesicherten Daten. | No safed data.'.LINE_FEED;
		$aenderungVorhanden = true;
	} else {
		$dataLocal = unserialize($dataLocal);
	}

	$dataRemote = new Data();
	$dataRemote->setRssCores(Parser::parseRssCore());
	$watchlist = array();
	$watchlist[] ='http://extensions.joomla.org/extensions/access-a-security/site-security/backup/1606';
	$watchlist[] ='http://extensions.joomla.org/extensions/photos-a-images/photo-gallery/13762';
	$dataRemote->setExtensions(Parser::parseExtensionHTML($watchlist));
	file_put_contents(CFG::DATEINAME_TEMP(),  serialize($dataRemote));

	if (!$aenderungVorhanden) {
		// compare
		$output .= 'Joomla-Core:'.LINE_FEED;
		foreach ($dataRemote->rssCores AS $name => $rssCoreRemote) {
			$rssCoreLocal = $dataLocal->getRssCore($name);

			if ($rssCoreLocal->name != $rssCoreRemote->name || //
					$rssCoreLocal->version != $rssCoreRemote->version || //
					$rssCoreLocal->downloadurl != $rssCoreRemote->downloadurl ) {
				$output .= 'Diff:'.LINE_FEED;
				$output .= '- name-local : '.$rssCoreLocal->name.LINE_FEED;
				$output .= '- name-remote: '.$rssCoreRemote->name.LINE_FEED;
				$output .= '- version-local : '.$rssCoreLocal->version.LINE_FEED;
				$output .= '- version-remote: '.$rssCoreRemote->version.LINE_FEED;
				$output .= '- downloadurl-local : '.$rssCoreLocal->downloadurl.LINE_FEED;
				$output .= '- downloadurl-remote: '.$rssCoreRemote->downloadurl.LINE_FEED;
				$aenderungVorhanden = true;
			} else {
				$output .= 'OK: '.$rssCoreLocal->name.' ('.$rssCoreLocal->version.')'.LINE_FEED;
			}
		}

		$output .= LINE_FEED.'Extensions:'.LINE_FEED;
		foreach ($dataLocal->extensions AS $name => $extensionLocal) {
			$extensionRemote = $dataRemote->getExtension($name);

			if ($extensionRemote->version != $extensionLocal->version) {
				$output .= 'Diff: '.$name.LINE_FEED;
				$output .= '- diff-url-local : '.$extensionLocal->url.LINE_FEED;
				$output .= '- diff-url-remote: '.$extensionRemote->url.LINE_FEED;
				$output .= '- diff-version-local : '.$extensionLocal->version.LINE_FEED;
				$output .= '- diff-version-remote: '.$extensionRemote->version.LINE_FEED;
				$output .= '- LINK: <a href="'.$extensionRemote->url.'">'.$extensionRemote->url.'</a>'.LINE_FEED;
				$aenderungVorhanden = true;
			} else {
				$output .= 'OK: '.$name.LINE_FEED;
			}
		}

	}

	if ($aenderungVorhanden) {
		$output .= LINE_FEED.LINE_FEED;
		$output .= '<A href="index.php?target=speichern">Aktuelle Daten fuer spaeteren Vergleich sichern. | Safe actual data for future compares.</A>';
		$output .= LINE_FEED.LINE_FEED;
	}

	echo $output;
}
?>