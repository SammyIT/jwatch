<?php
defined( '_VALID_ACCESS' ) or die( 'Unerlaubter Zugriff.');

include ("model.php");
include ("parser.php");

define('EMAIL_SENDER', 'sven@sven-henkel.de');
define('EMAIL_RECEIVER', 'sven-henkel@web.de');

class CronJWatch {
	static public function doit() {
		$aenderungenVorhanden = false;
		$nachricht = "";

		// Lokale Daten lesen
		$dataLocal = file_get_contents(CFG::DATEINAME());
		if ($dataLocal === FALSE) {
			$nachricht .= 'Keine gesicherten Daten. | No safed data.';
			$aenderungenVorhanden = true;
		} else {
			$dataLocal = unserialize($dataLocal);
		}

		// Remote-Daten lesen
		$dataRemote = new Data();
		$dataRemote->setRssCores(Parser::parseRssCore());
		$watchlist = array();
		$watchlist[] ='http://extensions.joomla.org/extensions/access-a-security/site-security/backup/1606';
		$watchlist[] ='http://extensions.joomla.org/extensions/photos-a-images/photo-gallery/13762';
		$dataRemote->setExtensions(Parser::parseExtensionHTML($watchlist));
		file_put_contents(CFG::DATEINAME_TEMP(),  serialize($dataRemote));
		
		// Vergleichen, wenn lokal Daten gefunden wurden.
		if (!$aenderungenVorhanden) {
			foreach ($dataRemote->rssCores AS $name => $rssCoreRemote) {
				$rssCoreLocal = $dataLocal->getRssCore($name);

				if ($rssCoreLocal->name != $rssCoreRemote->name || //
						$rssCoreLocal->version != $rssCoreRemote->version || //
						$rssCoreLocal->downloadurl != $rssCoreRemote->downloadurl ) {
					$nachricht .= 'Aenderungen im Core-RSS-Feed: '.$rssCoreLocal->name.'\n';
					$aenderungenVorhanden = true;
				}
			}

			foreach ($dataLocal->extensions AS $name => $extensionLocal) {
				$extensionRemote = $dataRemote->getExtension($name);

				if ($extensionRemote->version != $extensionLocal->version) {
					$nachricht .= "Aenderungen in Extensions: ".$name."\n";
					$aenderungenVorhanden = true;
				}
			}

		}
		
		// Mailen, wenn Aenderungen anliegen.
		if ($aenderungenVorhanden) {
			$titel = "J!Watch has detected changes";
			mail(EMAIL_RECEIVER, $titel, $nachricht, "From: ".EMAIL_SENDER."\r\n");
		}
	}
}
?>