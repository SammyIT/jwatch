<?php
defined( '_VALID_ACCESS' ) or die( 'Unerlaubter Zugriff.');

require "phpQuery.php";

class Parser {
	static public function parseRssCore() {
		$c = curl_init('http://update.joomla.org/core/extension.xml');
		curl_setopt($c, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($c, CURLOPT_HEADER, 0);
		$xmlstr = curl_exec($c);
		curl_close($c);
		$xml = new SimpleXMLElement($xmlstr);
		
		$list = array();
		foreach ($xml->update AS $elem) {
			$rssCore = new RssCore();
			
			$rssCore->name = $elem->name->asXML();
			$rssCore->version = $elem->version->asXML();
			$rssCore->downloadurl = $elem->downloadurl->asXML(); 

			$list[] = $rssCore;
		}
		
		return $list;
	}
	
	static public function parseExtensionHTML($watchlist) {
		$list = array();
		foreach ($watchlist as $link) {
			$c = curl_init($link);
			curl_setopt($c, CURLOPT_RETURNTRANSFER, 1);
			curl_setopt($c, CURLOPT_HEADER, 0);
			$xmlstr = curl_exec($c);
			curl_close($c);
			$doc = phpQuery::newDocumentHTML($xmlstr);

			$name = pq('#listing a:first')->text();
			
			$params = array();
			foreach (pq('#listing .fields_ext .caption') AS $param) {
				$params[pq($param)->text()] = pq($param)->next()->text(); 
			
			}
			
			$ext = new Extension();
			$ext->url = $link;
			$ext->name = $name;
			$ext->version = $params['Version'];
			
			$list[] = $ext;
		}
		
		return $list;
	}
}
?>
