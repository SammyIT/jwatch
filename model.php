<?php
	defined( '_VALID_ACCESS' ) or die( 'Unerlaubter Zugriff.');

	// Diese Datei muss eh jeder laden, deshalb hinterlegen wir die Konfiguration mal hier.
	class CFG {
		static public function DATEINAME() {
			return dirname(__FILE__).DIRECTORY_SEPARATOR."daten.dat";
		}
		static public function DATEINAME_TEMP() {
			return dirname(__FILE__).DIRECTORY_SEPARATOR."daten.tmp";
		}
	}
	
	class Data {
		public $rssCores = array();
		public $extensions = array();
		
		public function setRssCores($items) {
			// Hole RSS-Feed has to be the same
			foreach ($items AS $item) {
				$this->rssCores[] = $item;
			}
		}
		
		public function getRssCore($name) {
			return $this->rssCores[$name];
		}

		public function setExtensions($items) {
			foreach ($items AS $item) {
				$this->extensions[$item->name] = $item;
			}
		}
		
		public function getExtension($name) {
			return $this->extensions[$name];
		}
		
	}
	
	class RssCore {
		public $name = "";
		public $version = "";
		public $downloadurl = "";
	}
	
	
	class Extension {
		public $url = "";
		public $name = "";
		public $version = "";
	}
	
?>
